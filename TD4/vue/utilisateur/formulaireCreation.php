<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Formulaire</title>
</head>
<body>
<form method="get" action="../Controleur/routeur.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <label for="login_id">Login</label> :
            <input type="text" placeholder="leblancj" name="login" id="login_id" required/>
            <label for="nom_id">Nom</label> :
            <input type="text" placeholder="Leblanc" name="nom" id="nom_id" required/>
            <label for="prenom_id">Prenom</label> :
            <input type="text" placeholder="Juste" name="prenom" id="prenom_id" required/>
        </p>
        <p>
            <input type='hidden' name='action' value='creerDepuisFormulaire'>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>
</body>
</html>