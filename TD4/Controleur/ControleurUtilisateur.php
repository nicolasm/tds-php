<?php
require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        //require ('../vue/');  //"redirige" vers la vue
        ControleurUtilisateur::afficherVue('utilisateur/liste.php', ["utilisateurs" => $utilisateurs]);
    }

    public static function afficherDetail() : void {
        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login']); //appel au modèle pour gérer la BD
        if ($utilisateur == NULL) {
            ControleurUtilisateur::afficherVue('utilisateur/erreur.php');
        }
        else {
            ControleurUtilisateur::afficherVue('utilisateur/detail.php', ["utilisateur" => $utilisateur]);
        }
    }

    public static function afficherFormulaireCreation() : void {
        ControleurUtilisateur::afficherVue('utilisateur/formulaireCreation.php');
    }

    public static function creerDepuisFormulaire() : void {
        $utilisateur = new ModeleUtilisateur($_GET['login'], $_GET['nom'], $_GET['prenom']);
        $utilisateur->ajouter();
        ControleurUtilisateur::afficherListe();
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }
}
?>
