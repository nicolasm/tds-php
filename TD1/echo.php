<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP : 
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */
           
          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
          $texte = "hello world !";

          // On écrit le contenu de la variable 'texte' dans la page Web
          echo $texte;

          $prenom = "Marc";

          echo "Bonjour\n " . $prenom;
          echo "Bonjour\n $prenom";
          echo 'Bonjour\n $prenom';

          echo $prenom;
          echo "$prenom";
        ?>
        <br>
        <?php
            $nom = "Nicolas";
            $prenom = "Mael";
            $login = "nicolasm";

            echo "<p> Utilisateur $prenom $nom de login $login </p>";

            $utilisateur = [
                    'nom' => 'Nicolas',
                    'prenom' => 'Mael',
                    'login' => 'nicolasm'
            ];

            var_dump($utilisateur);

            echo "<p> Utilisateur $utilisateur[prenom] $utilisateur[nom] de login {$utilisateur['login']}</p>";

            // Ex8 Q4

            $utilisateur2 = [
                'nom' => 'Rivas',
                'prenom' => 'Raphael',
                'login' => 'rivasr'
            ];

            $utilisateur3 = [
                'nom' => 'Bons',
                'prenom' => 'Cyprien',
                'login' => 'bonsc'
            ];

            $utilisateurs = [
                    $utilisateur,
                    $utilisateur2,
                    $utilisateur3
            ];

            var_dump($utilisateurs);

            echo "<h1>Liste des utilisateurs :</h1>";
            echo '<ul>';
            for ($i = 0; $i < count($utilisateurs); $i++) {
                $util = $utilisateurs[$i];
                echo "<li>Utilisateur $util[nom] $util[prenom] de login $util[login]</li>";
            }
            echo '</ul>';

            if (!$utilisateurs) {
                echo "Il n'y a pas d'utilisateurs dans la liste";
            }

        ?>
    </body>
</html> 