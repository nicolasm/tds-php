<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees as ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use PDOException;

class UtilisateurRepository extends AbstractRepository
{
    protected function getNomTable(): string
    {
        return 'utilisateur';
    }

    public function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur {
        return new Utilisateur($utilisateurFormatTableau["login"], $utilisateurFormatTableau["nom"],
            $utilisateurFormatTableau["prenom"], $utilisateurFormatTableau["mdpHache"]);
    }

    protected function getNomClePrimaire(): string
    {
        return 'login';
    }

    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["login", "nom", "prenom", "mdpHache"];
    }


    protected function formatTableauSQL(AbstractDataObject $utilisateur): array
    {
        /** @var Utilisateur $utilisateur */
        return array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom(),
            "mdpHacheTag" => $utilisateur->getMdpHache(),
        );
    }



}