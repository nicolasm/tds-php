<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique
{
    protected static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    // Methode erreur générique

    public static function afficherFormulairePreference() : void {
        if (PreferenceControleur::existe()) {
            $controleur = PreferenceControleur::lire();
        }
        self::afficherVue('vueGenerale.php', ["titre" => "Controleur préféré",
            "cheminCorpsVue" => "formulairePreference.php", "controleurPref" => $controleur]);
    }

    public static function enregistrerPreference() : void {
        $controleur = $_GET['controleur_defaut'];
        PreferenceControleur::enregistrer($controleur);
        self::afficherVue('vueGenerale.php', ["titre" => "Controleur choisi",
            "cheminCorpsVue" => "preferenceEnregistree.php"]);
    }
}