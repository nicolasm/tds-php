<?php /** @var String $controleurPref */?>
<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
        <p>
            <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur"
                <?php if ($controleurPref == "utilisateur") {echo 'checked';} ?>>
            <label for="utilisateurId">Utilisateur</label>
        </p>
        <p>
            <input type="radio" id="trajetId" name="controleur_defaut" value="trajet"
                <?php if ($controleurPref == "trajet") {echo 'checked';} ?>>
            <label for="trajetId">Trajet</label>
        </p>
        <p>
            <input type='hidden' name='action' value='enregistrerPreference'>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>