<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../ressources/navstyle.css">
        <title><?php
            /**
             * @var string $titre
             */
            echo $titre; ?></title>
    </head>
    <body>
        <header>
            <nav>
                <ul>
                    <?php if (!\App\Covoiturage\Lib\ConnexionUtilisateur::estConnecte()) {
                        echo '<li>
                            <a href="controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur"><img src="../ressources/img/add-user.png"></a>
                        </li>
                        <li>
                            <a href="controleurFrontal.php?action=afficherFormulaireConnexion&controleur=utilisateur"><img src="../ressources/img/enter.png"></a>
                        </li>';
                    }
                    ?>
                    <li>
                        <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
                    </li><li>
                        <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
                    </li>
                    <li>
                        <a href="controleurFrontal.php?action=afficherFormulairePreference"><img src="../ressources/img/heart.png"></a>
                    </li>
                </ul>
            </nav>

        </header>
        <main>
            <?php
            /**
             * @var string $cheminCorpsVue
             */
            require __DIR__ . "/{$cheminCorpsVue}";
            ?>
        </main>
        <footer>
            <p>
                Site de covoiturage de Maël
            </p>
        </footer>
    </body>
</html>

