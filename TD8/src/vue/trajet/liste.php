<?php
/** @var Trajet[] $trajets */

foreach ($trajets as $trajet) {
    $idHTML = htmlspecialchars($trajet->getId());
    $idURL = rawurlencode($trajet->getId());

    echo '<p> Trajet d\'id : ' . '<a href="controleurFrontal.php?controleur=trajet&action=afficherDetail&id=' .
        $idURL . '">' . $idHTML . '</a>';
    echo '&nbsp;&nbsp;&nbsp;&nbsp;' . '<a href="controleurFrontal.php?controleur=trajet&action=afficherFormulaireMiseAJour&login=' . $idURL .
        '">Modifier</a>';
    echo '&nbsp;&nbsp;&nbsp;&nbsp;' . '<a href="controleurFrontal.php?controleur=trajet&action=supprimer&id=' . $idURL .
        '">Supprimer</a>' . '</p>';
}
echo '<p> <a href="controleurFrontal.php?controleur=trajet&action=afficherFormulaireCreation">' . 'Créer trajet' . '</a></p>';