<?php
/** @var Utilisateur[] $utilisateurs */

foreach ($utilisateurs as $utilisateur) {
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode($utilisateur->getLogin());

    echo '<p> Utilisateur de login ' . '<a href="controleurFrontal.php?action=afficherDetail&login=' .
        $loginURL . '">' . $loginHTML . '</a>.';
    echo '&nbsp;&nbsp;&nbsp;&nbsp;' . '<a href="controleurFrontal.php?action=afficherFormulaireMiseAJour&login=' . $loginURL .
        '">Modifier</a>';
    echo '&nbsp;&nbsp;&nbsp;&nbsp;' . '<a href="controleurFrontal.php?action=supprimer&login=' . $loginURL .
        '">Supprimer</a>' . '</p>';
}
echo '<p> <a href="controleurFrontal.php?action=afficherFormulaireCreation">' . 'Créer utilisateur' . '</a></p>';
?>
