<?php
    /** @var Utilisateur $utilisateur */
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $nomHTML = htmlspecialchars($utilisateur->getNom());
    $prenomHTML = htmlspecialchars($utilisateur->getPrenom());
?>

<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : leblancj" name="login" id="login_id"
                   value="<?= $loginHTML ?>" readonly required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Leblanc" name="nom" id="nom_id"
                   value="<?= $nomHTML ?>" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prenom&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Juste" name="prenom" id="prenom_id"
                   value="<?= $prenomHTML ?>" required/>
        </p>
        <p>
            <input type='hidden' name='action' value='mettreAJour'>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>