<?php
/** @var Trajet $trajet */
$departHTML = htmlspecialchars($trajet->getDepart());
$arriveeHTML = htmlspecialchars($trajet->getArrivee());
$dateHTML = htmlspecialchars($trajet->getdate()->format('Y-m-d'));
$prixHTML = htmlspecialchars($trajet->getprix());
$conducteurHTML = htmlspecialchars($trajet->getconducteur()->getLogin());
$nonFumeurHTML = htmlspecialchars($trajet->isnonfumeur());
?>

<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="depart_id">Depart&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Paris" value="<?= $departHTML ?>" name="depart" id="depart_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="arrivee_id">Arrivee&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Montpellier" value="<?= $arriveeHTML ?>" name="arrivee" id="arrivee_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="date_id">Date&#42;</label>
            <input class="InputAddOn-field" type="date" placeholder="Ex : 05/11/2023" value="<?= $dateHTML ?>" name="date" id="date_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prix">Prix&#42;</label>
            <input class="InputAddOn-field" type="number" placeholder="Ex : 23" value="<?= $prixHTML ?>" name="prix" id="prix_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="conducteur_id">ConducteurLogin&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : leblancj" value="<?= $conducteurHTML ?>" name="conducteurLogin" id="conducteurLogin_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nonFumeur_id">nonFumeur&#42;</label>
            <?php
                if($nonFumeurHTML){
                    echo '<input class="InputAddOn-field" type="checkbox" name="nonFumeur" id="nonFumeur_id" checked/>';
                }
                else {
                    echo '<input class="InputAddOn-field" type="checkbox" name="nonFumeur" id="nonFumeur_id"/>';
                }
            ?>
        </p>
        <p>
            <input type='hidden' name='action' value='mettreAJour'>
            <input type='hidden' name='controleur' value='trajet'>
            <input type='hidden' name='id' value="<?= $trajet->getId() ?>">
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>