<?php

namespace App\Covoiturage\Controleur;

//require_once __DIR__ . '/../Modele/Utilisateur.php'; // chargement du modèle
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur as Utilisateur;

class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        //appel au modèle pour gérer la BD
        //require ('../vue/');  //"redirige" vers la vue
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);
        //ControleurUtilisateur::afficherVue('utilisateur/liste.php', ["utilisateurs" => $utilisateurs]);
    }

    public static function afficherDetail() : void {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']); //appel au modèle pour gérer la BD
        if ($utilisateur == NULL) {
            //ControleurUtilisateur::afficherVue('utilisateur/erreur.php');
            //ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "Erreur utilisateur", "cheminCorpsVue" => "utilisateur/erreur.php"]);
            ControleurUtilisateur::afficherErreur("Le login utilisateur n'est pas valide");
        }
        else {
            ControleurUtilisateur::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Détails utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        }
    }

    public static function afficherFormulaireCreation() : void {
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "Formulaire création utilisateur", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire() : void {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository)->ajouter($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
    }

    public static function supprimer() : void {
        $login = $_GET['login'];
        if ((new UtilisateurRepository)->supprimer($login)) {
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            ControleurUtilisateur::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs,
                "login" => $login, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php"]);
        }
        else {
            self::afficherErreur("L'utilisateur de login $login n'a pas pu être supprimé.");
        }
    }

    public static function afficherFormulaireMiseAJour() : void {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        if ($utilisateur == NULL) {
            self::afficherErreur("Le login utilisateur n'existe pas.");
        }
        else {
            self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Formulaire mise à jour utilisateur",
                "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php"]);
        }
    }

    public static function mettreAJour() : void {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository)->mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs, "login" => $utilisateur->getLogin(), "titre" => "Liste des utilisateurs",
            "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"]);
    }

    public static function afficherErreur(string $messageErreur = "") : void {
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["messageErreur" => $messageErreur, "titre" => "Erreur", "cheminCorpsVue" => "utilisateur/erreur.php"]);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    /**
     * @return Utilisateur
     */
    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        return new Utilisateur($tableauDonneesFormulaire['login'], $tableauDonneesFormulaire['nom'], $tableauDonneesFormulaire['prenom']);
    }
}
?>
