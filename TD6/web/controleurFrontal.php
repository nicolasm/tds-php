<?php

use App\Covoiturage\Controleur\ControleurUtilisateur;
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

if (!isset($_GET['controleur'])) {
    $controleur = 'Utilisateur';
}
else {
    $controleur = $_GET['controleur'];
}

$nomDeClasseControleur = 'App\Covoiturage\Controleur\Controleur' . ucfirst($controleur);

if (!class_exists($nomDeClasseControleur)) {
    ControleurUtilisateur::afficherErreur("Le controleur n'existe pas.");
}

if (!isset($_GET['action'])) {
    $action = 'afficherListe';
}
else {
    $action = $_GET['action']; // On récupère l'action passée dans l'URL
}

if (!in_array($action, get_class_methods($nomDeClasseControleur))) {
    ControleurUtilisateur::afficherErreur("L'action n'est pas valide");
}

// Appel de la méthode statique $action de ControleurUtilisateur
$nomDeClasseControleur::$action();
?>
