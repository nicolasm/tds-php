<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class ControleurTrajet extends ControleurGenerique
{
    public static function supprimer() : void {
        $id = $_GET['id'];
        if ((new TrajetRepository)->supprimer($id)) {
            $trajets = (new TrajetRepository())->recuperer();
            ControleurTrajet::afficherVue('vueGenerale.php', ["trajets" => $trajets,
                "id" => $id, "titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/trajetSupprime.php"]);
        }
        else {
            self::afficherErreur("Le trajet d'id $id n'a pas pu être supprimé.");
        }
    }
    
    public static function afficherDetail() : void {
        $trajet = (new TrajetRepository())->recupererParClePrimaire($_GET['id']); //appel au modèle pour gérer la BD
        if ($trajet == NULL) {
            ControleurTrajet::afficherErreur("L'id trajet n'est pas valide");
        }
        else {
            ControleurTrajet::afficherVue('vueGenerale.php', ["trajet" => $trajet, "titre" => "Détails trajet", "cheminCorpsVue" => "trajet/detail.php"]);
        }
    }

    public static function afficherListe() : void {
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        //require ('../vue/');  //"redirige" vers la vue
        ControleurTrajet::afficherVue('vueGenerale.php', ["trajets" => $trajets, "titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/liste.php"]);
        //ControleurTrajet::afficherVue('trajet/liste.php', ["trajets" => $trajets]);
    }

    public static function afficherFormulaireCreation() : void {
        ControleurTrajet::afficherVue('vueGenerale.php', ["titre" => "Formulaire création trajet", "cheminCorpsVue" => "trajet/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire() : void {
        $trajet = self::construireDepuisFormulaire($_GET);

        (new TrajetRepository)->ajouter($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        ControleurTrajet::afficherVue('vueGenerale.php', ["trajets" => $trajets, "titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/trajetCree.php"]);
    }

    public static function afficherFormulaireMiseAJour() : void {
        $trajet = (new TrajetRepository())->recupererParClePrimaire($_GET['login']);
        if ($trajet == NULL) {
            self::afficherErreur("L'id trajet n'existe pas.");
        }
        else {
            self::afficherVue('vueGenerale.php', ["trajet" => $trajet, "titre" => "Formulaire mise à jour trajet",
                "cheminCorpsVue" => "trajet/formulaireMiseAJour.php"]);
        }
    }

    public static function mettreAJour() : void {
        $trajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository)->mettreAJour($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["trajets" => $trajets, "id" => $trajet->getId(), "titre" => "Liste des trajets",
            "cheminCorpsVue" => "trajet/trajetMisAJour.php"]);
    }

    public static function afficherErreur(string $messageErreur = "") : void {
        ControleurTrajet::afficherVue('vueGenerale.php', ["messageErreur" => $messageErreur, "titre" => "Erreur", "cheminCorpsVue" => "trajet/erreur.php"]);
    }

    /**
     * @return Trajet
     * @throws \Exception
     */
    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        /*
        $nonFumeur = false;
        if (isset($_GET['nonFumeur'])) {
            print("ICI");
            $nonFumeur = true;
        }
       */

        $nonFumeur = (isset($tableauDonneesFormulaire['nonFumeur'])) ?? false;
        $id = $tableauDonneesFormulaire["id"] ?? null;

        $trajet = new Trajet($id, $tableauDonneesFormulaire['depart'], $tableauDonneesFormulaire['arrivee'], new DateTime($tableauDonneesFormulaire['date']),
            $tableauDonneesFormulaire['prix'], (new UtilisateurRepository())->recupererParClePrimaire($tableauDonneesFormulaire['conducteurLogin']),
            $nonFumeur);

        //print($trajet->isNonFumeur());
        return $trajet;
    }
}