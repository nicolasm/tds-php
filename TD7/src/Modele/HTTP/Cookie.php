<?php

namespace App\Covoiturage\Modele\HTTP;

class Cookie
{
    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void {
        $valChaine = serialize($valeur);
        if (is_null($dureeExpiration)) {
            setcookie($cle, $valChaine);      // le cookie expire à la fin de la session
        }
        else {
            setcookie($cle, $valChaine, time() + $dureeExpiration);
        }
    }

    public static function lire(string $cle): mixed {
        return unserialize($_COOKIE[$cle]);
    }

    public static function contient($cle) : bool {
        return isset($_COOKIE[$cle]);
    }

    public static function supprimer($cle) : void {
        unset($_COOKIE[$cle]);
        setcookie ($cle, "", 1);
    }

}