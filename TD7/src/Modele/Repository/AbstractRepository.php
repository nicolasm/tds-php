<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use PDOException;

abstract class AbstractRepository
{
    public function mettreAJour(AbstractDataObject $objet): void
    {
        $table = $this->getNomTable();
        $colonnes = $this->getNomsColonnes();
        $expression = "";

        $premier = true;
        $deuxieme = true;
        foreach ($colonnes as $colonne) {
            if (!$premier && !$deuxieme) {
                $expression .= ", $colonne = :$colonne" . "Tag";
            }
            else if ($premier){
                $premier = false;
            }
            else {
                $expression .= "$colonne = :$colonne" . "Tag";
                $deuxieme = false;
            }
        }

        $sql = "UPDATE $table SET $expression WHERE $colonnes[0] = :$colonnes[0]Tag";

        //print($sql);
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = $this->formatTableauSQL($objet);

        $pdoStatement->execute($values);
    }

    public function ajouter(AbstractDataObject $objet): bool
    {
        $table = $this->getNomTable();
        $colonnes = join(', ', $this->getNomsColonnes());
        $colonnesValue = ':' . join('Tag, :', $this->getNomsColonnes()) . 'Tag';

        $sql = "INSERT INTO $table ($colonnes) VALUES ($colonnesValue)";
        //print($sql);
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = $this->formatTableauSQL($objet);

        /*
        foreach ($values as $ligne) {
            print("<p>$ligne</p>");
        }
        */

        try {
            $pdoStatement->execute($values);
        } catch (PDOException) {
            return false;
        }
        return true;
    }

    public function supprimer($valeurClePrimaire): bool
    {
        $table = $this->getNomTable();
        $cle = $this->getNomClePrimaire();

        $sql = "DELETE FROM $table WHERE $cle = :cleTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "cleTag" => $valeurClePrimaire,
        );

        if (!$this->recupererParClePrimaire($valeurClePrimaire)) return false;

        try {
            $pdoStatement->execute($values);
        } catch (PDOException) {
            return false;
        }
        return true;
    }

    public function recupererParClePrimaire(string $valeur): ?AbstractDataObject
    {
        $table = $this->getNomTable();
        $cle = $this->getNomClePrimaire();

        $sql = "SELECT * from $table WHERE $cle = :cleTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "cleTag" => $valeur,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $objectFormatTableau = $pdoStatement->fetch();

        if ($objectFormatTableau == []) {
            return null;
        }
        return $this->construireDepuisTableauSQL($objectFormatTableau);
    }

    /**
     * @return AbstractDataObject[]
     */
    public function recuperer(): array
    {
        $dataObjects = [];
        $table = $this->getNomTable();
        $PDO = ConnexionBaseDeDonnees::getPdo();
        $pdoStatement = $PDO->query("SELECT * FROM $table");
        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $dataObject = $this->construireDepuisTableauSQL($utilisateurFormatTableau);
            $dataObjects[] = $dataObject;
        }
        return $dataObjects;
    }

    protected abstract function getNomTable(): string;

    protected abstract function getNomClePrimaire() : string;

    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;

    /** @return string[] */
    protected abstract function getNomsColonnes(): array;

    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;
}