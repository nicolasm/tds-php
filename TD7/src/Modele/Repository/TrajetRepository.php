<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees as ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Trajet;
use DateTime;
use PDOException;

class TrajetRepository extends AbstractRepository
{
    protected function getNomTable() : string
    {
        return 'trajet';
    }

    protected function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]),
            $trajetTableau["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($trajetTableau["conducteurLogin"]),
            $trajetTableau["nonFumeur"],
        );

        $trajet->setPassagers(TrajetRepository::recupererPassagers($trajet));
        return $trajet;
    }

    /**
     * @return Trajet[]
     */
    /*
    public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = TrajetRepository::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }
    */

    /**
     * @return Utilisateur[]
     */
    static public function recupererPassagers(Trajet $trajet): array {
        $utilisateurs = [];
        $id = $trajet->getId();
        $PDO = ConnexionBaseDeDonnees::getPdo();
        $pdoStatement = $PDO->query(
            "SELECT * FROM utilisateur u JOIN passager p ON p.passagerLogin = u.login JOIN trajet t ON t.id = p.trajetId WHERE trajetId = $id");
        foreach($pdoStatement as $utilisateurFormatTableau){
            $utilisateur = (new UtilisateurRepository())->construireDepuisTableauSQL($utilisateurFormatTableau);
            $utilisateurs[] = $utilisateur;
        }
        return $utilisateurs;
    }

    protected function getNomClePrimaire(): string
    {
        return 'id';
    }

    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee", "date", "prix", "conducteurLogin", "nonFumeur"];
    }

    protected function formatTableauSQL(AbstractDataObject $trajet): array
    {
        /** @var Trajet $trajet */
        return array(
            "idTag" => $trajet->getId(),
            "departTag" => $trajet->getDepart(),
            "arriveeTag" => $trajet->getArrivee(),
            "dateTag" => $trajet->getDate()->format("Y-m-d"),
            "prixTag" => $trajet->getPrix(),
            "conducteurLoginTag" => $trajet->getConducteur()->getLogin(),
            "nonFumeurTag" => $trajet->isNonFumeur(),
        );
    }


}