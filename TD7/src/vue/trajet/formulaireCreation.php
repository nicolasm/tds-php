<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="depart_id">Depart&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Paris" name="depart" id="depart_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="arrivee_id">Arrivee&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Montpellier" name="arrivee" id="arrivee_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="date_id">Date&#42;</label>
            <input class="InputAddOn-field" type="date" placeholder="Ex : 05/11/2023" name="date" id="date_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prix">Prix&#42;</label>
            <input class="InputAddOn-field" type="number" placeholder="Ex : 23" name="prix" id="prix_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="conducteur_id">ConducteurLogin&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : leblancj" name="conducteurLogin" id="conducteurLogin_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nonFumeur_id">nonFumeur&#42;</label>
            <input class="InputAddOn-field" type="checkbox" name="nonFumeur" id="nonFumeur_id"/>
        </p>
        <p>
            <input type='hidden' name='action' value='creerDepuisFormulaire'>
            <input type='hidden' name='controleur' value='trajet'>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>