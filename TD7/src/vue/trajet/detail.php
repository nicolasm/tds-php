<?php

/** @var Trajet $trajet */

$conducteur = $trajet->getConducteur();
$nomHTML = htmlspecialchars($conducteur->getNom());
$prenomHTML = htmlspecialchars($conducteur->getPrenom());
$nonFumeurBool = $trajet->isNonFumeur();
$date = $trajet->getDate()->format("d/m/Y");
$departHTML = htmlspecialchars($trajet->getDepart());
$arriveeHTML = htmlspecialchars($trajet->getArrivee());


$nonFumeur = $nonFumeurBool ? " non fumeur" : " ";
echo "<p>Le trajet$nonFumeur du {$date} partira de {$departHTML} 
pour aller à {$arriveeHTML} (conducteur: {$prenomHTML} 
{$nomHTML}).</p>";
