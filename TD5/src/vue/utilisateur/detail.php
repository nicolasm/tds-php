<?php
/** @var ModeleUtilisateur $utilisateur */

$loginHTML = htmlspecialchars($utilisateur->getLogin());
$nomHTML = htmlspecialchars($utilisateur->getNom());
$prenomHTML = htmlspecialchars($utilisateur->getPrenom());

echo '<p> Utilisateur ' . $prenomHTML . ' ' . $nomHTML .
    ' de login ' . $loginHTML . '.</p>';
?>
