<?php

namespace App\Covoiturage\Controleur;

//require_once __DIR__ . '/../Modele/ModeleUtilisateur.php'; // chargement du modèle
use App\Covoiturage\Modele\ModeleUtilisateur as ModeleUtilisateur;

class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        //require ('../vue/');  //"redirige" vers la vue
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);
        //ControleurUtilisateur::afficherVue('utilisateur/liste.php', ["utilisateurs" => $utilisateurs]);
    }

    public static function afficherDetail() : void {
        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login']); //appel au modèle pour gérer la BD
        if ($utilisateur == NULL) {
            ControleurUtilisateur::afficherVue('utilisateur/erreur.php');
            ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "Erreur utilisateur", "cheminCorpsVue" => "utilisateur/erreur.php"]);
        }
        else {
            ControleurUtilisateur::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Détails utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        }
    }

    public static function afficherFormulaireCreation() : void {
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "Formulaire création utilisateur", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire() : void {
        $utilisateur = new ModeleUtilisateur($_GET['login'], $_GET['nom'], $_GET['prenom']);
        $utilisateur->ajouter();
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }
}
?>
